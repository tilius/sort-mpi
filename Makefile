
all: sort sort_seq

FLAGS=-Wall -Wshadow -Wextra -lm -g

sort: sort.cpp
	mpic++ -o $@ $< $(FLAGS)

sort_seq: sort_seq.cpp
	g++ -o sort_seq sort_seq.cpp $(FLAGS)
