
max=7
for e in `seq 1 $max`; do
	nodes=$[2**(e)]
	chunk=$[10000*2**(max-e)]
	echo -n "$nodes $chunk "
	./local_run.sh $nodes $chunk | sort -n > tmp.out
	storage_reads=`cat tmp.out | tail -1 | awk '{print $2}'`
	max_compute_reads=`cat tmp.out | head -$[2**(e-1)] | awk '{max = (max>$2)?max:$2;} END {print max}'`
	storage_writes=`cat tmp.out | tail -1 | awk '{print $3}'`
	max_compute_writes=`cat tmp.out | head -$[2**(e-1)] | awk '{max = (max>$3)?max:$3;} END {print max}'`
	storage_cmps=`cat tmp.out | tail -1 | awk '{print $4}'`
	max_compute_cmps=`cat tmp.out | head -$[2**(e-1)] | awk '{max = (max>$4)?max:$4;} END {print max}'`
	echo $storage_reads $max_compute_reads $storage_writes $max_compute_writes $storage_cmps $max_compute_cmps
done

