
#include <cstdlib>
#include <cstdio>

const int N = 1 << 24;
int A[N];
int temp[N];

void mergesort(int list[], int left, int right) {
	if (left < right) {
		int i, j, k, t, middle;
		middle = (left + right) / 2;
		mergesort(list, left, middle);
		mergesort(list, middle+1, right);
		k = i = left; j = middle + 1;
		while (i <= middle && j <= right)
			temp[k++] = list[i] < list[j] ? list[i++] : list[j++];
		t = i > middle ? j : i;
		while (k <= right)
			temp[k++] = list[t++];
		for (k = left; k <= right; k++)
			list[k] = temp[k];
	}
}

int main() {
	srand(0);
	for (int i = 0; i < N; i++)
		A[i] = rand() % N;
	mergesort(A, 0, N-1);
}

