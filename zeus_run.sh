#!/bin/sh
#PBS -l walltime=0:03:00
#PBS -l pmem=64mb
#PBS -l nodes=16:ppn=12

module load mvapich2
module load mpiexec
cd /people/plgtilius/sort-mpi
make sort || exit 1
exec > sort.out
for e in {0..10}; do
	N=$[2**e]
	echo -n "$N "
	mpiexec -np $[N*2] ./sort || exit 2
done

