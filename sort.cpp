#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <algorithm>
using std::sort;

typedef unsigned long long LL;

int rank, world_size, half;

int CHUNK = 1 << 20;

const int
	SEND_ME = 0,
	UPDATE_YOURS = 1,
	SORT_DATA = 2,
	SYNC_DATA = 3,
	DO_BARRIER = 4,
	TERMINATE = 5;

MPI_Status recv_status;

int recv_cnt, send_cnt, cmp_cnt;

void recv(int* data, int count, int target) {
	recv_cnt += count;
	MPI_Recv(data, count, MPI_INT, target, 0, MPI_COMM_WORLD, &recv_status);
}

void send(const int* data, int count, int target) {
	send_cnt += count;
	MPI_Send((void*)data, count, MPI_INT, target, 0, MPI_COMM_WORLD);
}

void do_recv(int* data, int count, int target) {
	send(&SEND_ME, 1, target);
	recv(data, count, target);
}

void do_send(const int* data, int count, int target) {
	//printf("Compute node #%i sending to storage node #%i - CODE\n", rank, target);
	send(&UPDATE_YOURS, 1, target);
	//printf("Compute node #%i sending to storage node #%i - DATA\n", rank, target);
	send(data, count, target);
	//printf("Compute node #%i sending to storage node #%i - DONE\n", rank, target);
}

void send_to_all_storage(int code) {
	for (int t = half; t < world_size; t++)
		send(&code, 1, t);
}

void send_to_storages(int code, int from, int to) {
	for (int t = from; t <= to; t++)
		send(&code, 1, t);
}

void send_to_my_storage(int code) {
	send(&code, 1, rank + half);
}

struct InputBuffer {
	int from, to, current, pos;
	int* data;

	InputBuffer(int _from, int _to): from(_from), to(_to), current(_from), pos(0), data(new int[CHUNK]) {
		fetch();
	}

	~InputBuffer() { delete[] data; }
	
	void fetch() {
		do_recv(data, CHUNK, current);
	}

	int peek() {
		return data[pos];
	}

	int poll() {
		int res = data[pos];
		if (++pos == CHUNK) {
			if (++current <= to)
				fetch();
			pos = 0;
		}
		return res;
	}

	bool eod() {
		return to < current;
	}
};

struct OutputBuffer {
	int from, to, current, pos;
	int* data;

	OutputBuffer(int _from, int _to): from(_from), to(_to), current(_from), pos(0), data(new int[CHUNK]) {}
	~OutputBuffer() { delete[] data; }

	void operator << (int value) {
		data[pos] = value;
		if (++pos == CHUNK) {
			do_send(data, CHUNK, current);
			current++;
			pos = 0;
		}
	}
};

void test_basic() {
	int data[CHUNK];

	do_send(data, CHUNK, rank+half);
	if (rank == 0)
		send_to_all_storage(DO_BARRIER);
	MPI_Barrier(MPI_COMM_WORLD);

	do_recv(data, CHUNK, rank+half);
	if (rank == 0)
		send_to_all_storage(DO_BARRIER);
	MPI_Barrier(MPI_COMM_WORLD);
}

void test_buffers() {
	if (rank == 0) {
		InputBuffer ib(half, half+half/2-1);
		OutputBuffer ob(half+half/2, world_size-1);
		while (!ib.eod())
			ob << ib.poll();
	}
}

int l2(int v) {
	return v ? l2(v >> 1) + 1 : 0;
}

int trailing_ones(int v) {
	return v & 1 ? trailing_ones(v >> 1) + 1 : 0;
}

void print_all() {
	if (rank == 0) {
		for (int i = 0; i < half; i++) {
			InputBuffer b(half + i, half + i);
			printf("Chunk #%i: ", i);
			while (!b.eod())
				printf("%i, ", b.poll());	
			puts("");
		}
		send_to_all_storage(DO_BARRIER);
	}
	MPI_Barrier(MPI_COMM_WORLD);
}

void verify() {
	int last = 0;
	if (rank == 0) {
		for (int i = 0; i < half; i++) {
			InputBuffer b(half + i, half + i);
			while (!b.eod()) {
				int cur = b.poll();
				if (cur < last)
					printf("Error in storage chunk #%i!\n", i);
				last = cur;
			}
		}
		send_to_all_storage(DO_BARRIER);
	}
	MPI_Barrier(MPI_COMM_WORLD);
}

void mergesort() {

	send_to_my_storage(SORT_DATA);
	send_to_my_storage(DO_BARRIER);
	MPI_Barrier(MPI_COMM_WORLD);

	int my_iters = trailing_ones(rank);
	int total_iters = trailing_ones(half - 1);

	for (int i = 0; i < total_iters; i++) {
		if (i < my_iters) {
			int shift = 1 << i;
			// do the merge with node no. (rank-shift)
			int start = half + rank - 2 * shift + 1;
			int mid = start + shift;
			int end = mid + shift - 1;

			//printf("Rank %i, iter %i: [%i, %i]\n", rank, i, start - half, end - half);
			
			InputBuffer i1(start, mid - 1);
			InputBuffer i2(mid, end);
			OutputBuffer o(start, end);

			while (!i1.eod() && !i2.eod()) {
				int v1 = i1.peek();
				int v2 = i2.peek();
				cmp_cnt++;
				o << (v1 < v2 ? i1 : i2).poll();
			}
			while (!i1.eod())
				o << i1.poll();
			while (!i2.eod())
				o << i2.poll();

			send_to_storages(SYNC_DATA, start, end);
			send_to_storages(DO_BARRIER, start, end);
		}

		MPI_Barrier(MPI_COMM_WORLD);
		//print_all();
	}
}

int main(int argc, char** argv) {
	MPI_Init(NULL, NULL);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);

	if ((world_size & (world_size - 1)) != 0) {
		fprintf(stderr, "World size must be a power of two!\n");
		MPI_Abort(MPI_COMM_WORLD, 1);
		return 0;
	}

	half = world_size / 2;

	if (argc == 2) {
		CHUNK = atoi(argv[1]);
	}

	MPI_Barrier(MPI_COMM_WORLD);
	double start = MPI_Wtime(), end;

	if (rank < half) { // compute nodes
		//print_all();
		mergesort();
		verify();
		//print_all();
		//test_basic();
		//test_buffers();

	} else { // storage nodes
		int data[CHUNK], new_data[CHUNK];
		for (int i = 0; i < CHUNK; i++)
			data[i] = (world_size - rank - 1) * CHUNK + CHUNK - i;

		bool run = true;
		while (run) {
			int code;
			recv(&code, 1, MPI_ANY_SOURCE);
			int src = recv_status.MPI_SOURCE;
			//printf("Rank = %i, received code %i from %i\n", rank, code, src);
			switch (code) {
				case SEND_ME:
					//printf("Rank = %i, received SEND_ME (now send to %i)\n", rank, src);
					send(data, CHUNK, src);
					break;
				case UPDATE_YOURS:
					//printf("Rank = %i, received UPDATE_YOURS (now recv from %i)\n", rank, src);
					recv(new_data, CHUNK, src);
					break;
				case SORT_DATA:
					//printf("Rank = %i, received SORT_DATA\n", rank);
					cmp_cnt = CHUNK * l2(CHUNK);
					sort(data, data + CHUNK);
					break;
				case SYNC_DATA:
					//printf("Rank = %i, received SYNC_DATA\n", rank);
					memcpy(data, new_data, CHUNK * sizeof(int));
					break;
				case DO_BARRIER:
					//printf("Rank = %i, received DO_BARRIER\n", rank);
					MPI_Barrier(MPI_COMM_WORLD);
					break;
				case TERMINATE:
					//printf("Rank = %i, received TERMINATE\n", rank);
					run = false;
					break;
			}
		}
	}

	if (rank == 0)
		send_to_all_storage(TERMINATE);

	end = MPI_Wtime();
	if (rank == -1)
		printf("%lf\n", end - start);
	printf("%i %i %i %i\n", rank, recv_cnt, send_cnt, cmp_cnt);

	MPI_Finalize();
	return 0;
}

